from content import views
from rest_framework import routers

from django.urls import path, include


router = routers.SimpleRouter()
router.register('posts', views.PostViewSet, basename='Post')

urlpatterns = [
    path('', include(router.urls)),
    path('like/<int:post_id>/', views.LikePost.as_view()),
    path('unlike/<int:post_id>/', views.UnLikePost.as_view()),
    path('comment/<int:post_id>/', views.CommentOnPost.as_view()),
]
from content.models import Post, Like, Comment
from rest_framework import serializers


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = '__all__'
        read_only_fields = ('owner',)

class CommentModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ('id','text','created_at','post')

class CommentSerializer(serializers.Serializer):
    text = serializers.CharField(required=True, allow_blank=False, max_length=100)
import bleach

from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from content.models import Comment, Like, Post
from content.serializers import CommentModelSerializer, CommentSerializer, PostSerializer

from rest_framework import serializers, viewsets
from rest_framework.response import Response
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt import authentication


class PostViewSet(viewsets.ModelViewSet):
    authentication_classes = [authentication.JWTAuthentication]
    permission_classes = [IsAuthenticated]
    
    queryset = Post.objects.all()
    serializer_class = PostSerializer

    def get_queryset(self):
        queryset = self.queryset
        query_set = queryset.filter(owner=self.request.user)
        return query_set

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)
    
    def list(self, request, *args, **kwargs):
        custom_data = []
        for post in self.get_serializer(self.get_queryset(),many=True).data:
            post['likes'] = Like.objects.filter(post__id=post['id']).count()
            post['comments_count'] = Comment.objects.filter(post__id=post['id']).count()
            comments = Comment.objects.filter(post__id=post['id'])
            post['comments'] = CommentModelSerializer(comments, many=True).data
            custom_data.append(post)
        
        return Response(custom_data)
    
    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        
        post = serializer.data
        post['likes'] = Like.objects.filter(post__id=post['id']).count()
        post['comments_count'] = Comment.objects.filter(post__id=post['id']).count()
        comments = Comment.objects.filter(post__id=post['id'])
        post['comments'] = CommentModelSerializer(comments, many=True).data
        
        return Response(post)


class LikePost(APIView):
    authentication_classes = [authentication.JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request, post_id):
        """
        Like a Post
        """
        post_instance = get_object_or_404(Post, id=post_id)
        obj_like, liked = Like.objects.get_or_create(
            post=post_instance,
            owner=request.user,
        )
        message = f"You have already liked post {post_id}"
        if liked:
            message = f"You liked post {post_id}"
        
        response = {
            "message": message,
        }

        return Response(response)


class UnLikePost(APIView):
    authentication_classes = [authentication.JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request, post_id):
        """
        UnLike a Post
        """
        post_instance = get_object_or_404(Post, id=post_id)
        like_instance = get_object_or_404(Like, post=post_instance, owner=request.user)
        like_instance.delete()

        response = {
            "message": f"You unliked post {post_id}",
        }

        return Response(response)


class CommentOnPost(APIView):
    authentication_classes = [authentication.JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request, post_id):
        """
        Comment on a Post
        """
        serializer = CommentSerializer(data=request.data)
        
        if not serializer.is_valid():
            return Response(serializer.errors, status=400)

        post_instance = get_object_or_404(Post, id=post_id)
        obj_comment = Comment.objects.create(
            post=post_instance,
            owner=request.user,
            text=bleach.clean(request.data.get("text"))
        )
        
        response = {
            "comment_id": obj_comment.id,
            "message": f"You commended on post {post_id}",
        }

        return Response(response)
from django.db import models
from django.contrib.auth.models import User


class Followers(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_being_followed')
    follower = models.ForeignKey(User, on_delete=models.CASCADE, related_name='following_user')
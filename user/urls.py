from user import views

from django.urls import path, include


urlpatterns = [
    path('user/', views.UserProfileView.as_view()),
    path('follow/<int:user_id>/', views.FollowUser.as_view()),
    path('unfollow/<int:user_id>/', views.UnFollowUser.as_view()),
]
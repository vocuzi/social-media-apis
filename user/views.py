from django.contrib.auth.models import User
import user
from user.models import Followers

from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt import authentication
from rest_framework.permissions import IsAuthenticated


class UserProfileView(APIView):
    authentication_classes = [authentication.JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        """
        Return current user's profile.
        """
        response = {
            "username": f"{request.user.first_name} {request.user.last_name}",
            "followers": Followers.objects.filter(user=request.user).count(),
            "following": Followers.objects.filter(follower=request.user).count(),
        }

        return Response(response)


class FollowUser(APIView):
    authentication_classes = [authentication.JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request, user_id):
        """
        Follow a user
        """
        user_instance = get_object_or_404(User, id=user_id)
        obj_follow, followed = Followers.objects.get_or_create(
            user=user_instance,
            follower=request.user,
        )
        message = f"You are already following {user_instance.id}"
        if followed:
            message = f"You are now following {user_instance.id}"
        
        response = {
            "message": message,
        }

        return Response(response)


class UnFollowUser(APIView):
    authentication_classes = [authentication.JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request, user_id):
        """
        UnFollow a user
        """
        user_instance = get_object_or_404(User, id=user_id)
        follower_instance = get_object_or_404(Followers, user=user_instance, follower=request.user)
        follower_instance.delete()

        response = {
            "message": f"Unfollowed {user_instance.id}",
        }

        return Response(response)